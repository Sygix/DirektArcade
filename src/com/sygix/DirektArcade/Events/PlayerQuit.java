/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Events;

import java.sql.SQLException;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sygix.DirektArcade.DirektArcade;
import com.sygix.DirektArcade.Runnables.ElytraRace;
import com.sygix.DirektArcade.Runnables.FallingAnvil;
import com.sygix.DirektArcade.Runnables.LobbyEnd;
import com.sygix.DirektArcade.Runnables.MonkeyGame;
import com.sygix.DirektArcade.Runnables.PushMe;
import com.sygix.DirektArcade.Runnables.stop;
import com.sygix.DirektArcade.SQL.SQLGestion;
import com.sygix.DirektArcade.Utils.GamePrefix;
import com.sygix.DirektArcade.Utils.GameState;

public class PlayerQuit implements Listener {
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		int playercount = Bukkit.getServer().getOnlinePlayers().size()-1;
		e.setQuitMessage("");
		try {
			SQLGestion.setJoueurs(Bukkit.getServerName(), playercount);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if(!DirektArcade.PlsNBR.contains(p)){
			return;
		}
		DirektArcade.PlsNBR.remove(p);
		//Bukkit.getServer().broadcastMessage("debug 9");
		if(GameState.isState(GameState.GAME)){
			if(DirektArcade.PlsNBR.size() < 2){
				if(DirektArcade.Game == "MonkeyGame"){
					MonkeyGame.stop();
				}
				if(DirektArcade.Game == "FallingAnvil"){
					FallingAnvil.stop();
				}
				if(DirektArcade.Game == "PushMe"){
					PushMe.stop();
				}
				if(DirektArcade.Game == "ElytraRace"){
					ElytraRace.stop();
				}
				
			}
		}
		for ( Player pls : Bukkit.getServer().getOnlinePlayers()){
			IChatBaseComponent comp = ChatSerializer.a("{\"text\": \""+GamePrefix.getGamePrefix()+ChatColor.GRAY+""+p.getName()+" a quitt� le jeu !"+" ( "+playercount+"/13 )"+"\"}");
			PacketPlayOutChat paction = new PacketPlayOutChat(comp, (byte) 2);
			((CraftPlayer) pls).getHandle().playerConnection.sendPacket(paction);
		}
		if(playercount < 3){
			LobbyEnd.Timerstop();
		}
		if(playercount < 1){
			stop.Stop();
		}
	}
}
