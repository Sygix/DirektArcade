/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Events;

import java.sql.SQLException;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.sygix.DirektArcade.Runnables.LobbyEnd;
import com.sygix.DirektArcade.SQL.SQLGestion;
import com.sygix.DirektArcade.Utils.GamePrefix;
import com.sygix.DirektArcade.Utils.GameState;
import com.sygix.DirektArcade.Utils.scoreboard;

public class PlayerJoin implements Listener {
	
	@EventHandler
	public static void onPlayerJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		if((GameState.isState(GameState.GAME))||(GameState.isState(GameState.PREGAME))||(GameState.isState(GameState.FINISH))){
			p.kickPlayer(ChatColor.RED+"La partie est d�j� commenc�e !");
			e.setJoinMessage("");
			return;
		}
		p.teleport(new Location(Bukkit.getWorlds().get(0), 0.5, 4, 0.5));
		p.setGameMode(GameMode.ADVENTURE);
		p.setHealth(20);
		p.setFoodLevel(20);
		p.getInventory().clear();
		scoreboard.team(p);
		int playercount = Bukkit.getServer().getOnlinePlayers().size();
		try {
			SQLGestion.setJoueurs(Bukkit.getServerName(), playercount);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		e.setJoinMessage("");
		p.sendMessage(ChatColor.AQUA+"--------------------\n"+ChatColor.YELLOW+"Vous devez voter /vote <1/2/3/4>\n1. PushMe\n2. FallingAnvil\n3. ElytraRace\n4. MonkeyGame\n"+ChatColor.AQUA+"--------------------");
		p.sendMessage(ChatColor.GRAY+""+ChatColor.ITALIC+"Vous pouvez partager votre halo avec la commande /hshare.\n Si vous quittez une partie en cours, vous ne gagnerez pas de DirektCoins.");
		for ( Player pls : Bukkit.getServer().getOnlinePlayers()){
			IChatBaseComponent comp = ChatSerializer.a("{\"text\": \""+GamePrefix.getGamePrefix()+ChatColor.GREEN+""+p.getName()+" a rejoint le jeu !"+" ( "+playercount+"/13 )"+"\"}");
			PacketPlayOutChat paction = new PacketPlayOutChat(comp, (byte) 2);
			((CraftPlayer) pls).getHandle().playerConnection.sendPacket(paction);
		}
		if(playercount < 2){
			p.sendMessage(GamePrefix.getGamePrefix()+ChatColor.RED+"Il faut minimum 2 joueurs pour d�marrer.");
		}
		if(playercount >= 2){
			LobbyEnd.Timer();
		}
	}

}
