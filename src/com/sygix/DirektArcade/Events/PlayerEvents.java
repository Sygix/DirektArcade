/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import com.sygix.DirektArcade.DirektArcade;
import com.sygix.DirektArcade.Runnables.ElytraRace;
import com.sygix.DirektArcade.Runnables.FallingAnvil;
import com.sygix.DirektArcade.Runnables.MonkeyGame;
import com.sygix.DirektArcade.Runnables.PushMe;
import com.sygix.DirektArcade.Utils.BroadcastMessage;
import com.sygix.DirektArcade.Utils.GamePrefix;
import com.sygix.DirektArcade.Utils.GameState;

@SuppressWarnings("deprecation")
public class PlayerEvents implements Listener {
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e){
		if((GameState.isState(GameState.LOBBY)) || (GameState.isState(GameState.PREGAME)) || (GameState.isState(GameState.FINISH))){
			e.setDamage(0);
		}
		if(DirektArcade.Game == "PushMe"){
			e.setDamage(0);
		}
		if(DirektArcade.Game == "MonkeyGame"){
			if(MonkeyGame.time > 270){
				e.setCancelled(true);
			}else{
				e.setDamage(0);
			}
		}
		if(DirektArcade.Game == "ElytraRace"){
			e.setCancelled(true);
		}
		if(DirektArcade.Game == "FallingAnvil"){
			e.getCause();
			if(e.getEntity() instanceof Player){
				Player p = (Player) e.getEntity();
				if(e.getCause().equals(DamageCause.FALLING_BLOCK)){
					DirektArcade.PlsNBR.remove(p);
					p.setGameMode(GameMode.SPECTATOR);
					p.sendMessage(GamePrefix.getGamePrefix()+ChatColor.RED+"Vous avez perdu !");
					p.getInventory().clear();
					BroadcastMessage.broadcast(GamePrefix.getGamePrefix()+ChatColor.RED+p.getDisplayName()+" s'est pris une enclume sur la t�te !");
					if(DirektArcade.PlsNBR.size() >= 2){
						for(Player pls : Bukkit.getServer().getOnlinePlayers()){
							pls.playSound(pls.getLocation(), Sound.ENTITY_ELDER_GUARDIAN_AMBIENT, 1F, 1F);
						}
					}
					if(DirektArcade.PlsNBR.size() < 2){
						FallingAnvil.stop();
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		int b = p.getLocation().getBlockY();
		Location loc = p.getLocation();
		Block bl = p.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ());
		if(DirektArcade.Game == "PushMe"){
			if(p.getGameMode().equals(GameMode.ADVENTURE)){
				if(b < 40){
					DirektArcade.PlsNBR.remove(p);
					p.setGameMode(GameMode.SPECTATOR);
					p.sendMessage(GamePrefix.getGamePrefix()+ChatColor.RED+"Vous avez perdu !");
					p.getInventory().clear();
					BroadcastMessage.broadcast(GamePrefix.getGamePrefix()+ChatColor.RED+p.getDisplayName()+" s'est perdu dans le vide !");
					if(DirektArcade.PlsNBR.size() >= 2){
						for(Player pls : Bukkit.getServer().getOnlinePlayers()){
							pls.playSound(pls.getLocation(), Sound.ENTITY_ELDER_GUARDIAN_AMBIENT, 1F, 1F);
						}
					}
					if(DirektArcade.PlsNBR.size() < 2){
						PushMe.stop();
					}
				}
			}
		}
		if(DirektArcade.Game == "MonkeyGame"){
			if(p.getGameMode().equals(GameMode.ADVENTURE)){
				if(b >= 256){
					for(Player pls : Bukkit.getServer().getOnlinePlayers()){
						pls.setGameMode(GameMode.SPECTATOR);
						pls.getInventory().clear();
						if(pls != p){
							DirektArcade.PlsNBR.remove(pls);
						}
					}
					BroadcastMessage.broadcast(GamePrefix.getGamePrefix()+ChatColor.RED+p.getDisplayName()+" est le premier arriv� en haut de son Arbre !");
					MonkeyGame.stop();
				}
			}
		}
		if(DirektArcade.Game == "ElytraRace"){
			if(p.getGameMode().equals(GameMode.ADVENTURE)){
				if(!p.isGliding()){
					if ((bl != null) && (bl.getType().equals(Material.DIAMOND_BLOCK))){
						for(Player pls : Bukkit.getServer().getOnlinePlayers()){
							pls.setGameMode(GameMode.SPECTATOR);
							pls.getInventory().clear();
							if(pls != p){
								DirektArcade.PlsNBR.remove(pls);
							}
						}
						BroadcastMessage.broadcast(GamePrefix.getGamePrefix()+ChatColor.RED+p.getDisplayName()+" a gagn� la course !");
						ElytraRace.stop();
						return;
					}
					if ((bl != null) && (!bl.getType().equals(Material.AIR))){
						p.teleport(new Location(Bukkit.getWorlds().get(0), -999.5, 255, 1000.5));
						p.setGliding(true);
						p.sendMessage(GamePrefix.getGamePrefix()+ChatColor.RED+"Vous devez planner avec vos Ailes !");
					}
				}else{
					if ((bl != null) && (bl.getType().equals(Material.GOLD_BLOCK))){
						p.setVelocity(new Vector(loc.getDirection().getX(), 0.1, loc.getDirection().getZ()).multiply(2.5));
					}
					if ((bl != null) && (bl.getType().equals(Material.QUARTZ_BLOCK))){
						p.setVelocity(new Vector(loc.getDirection().getX(), loc.getDirection().getY()+0.4, loc.getDirection().getZ()));
					}
				}
			}
		}
	}
	
	@EventHandler
	public void Inventoryclick(InventoryClickEvent e){
		if((GameState.isState(GameState.LOBBY)) || (GameState.isState(GameState.PREGAME)) || (GameState.isState(GameState.FINISH))){
			e.setCancelled(true);
		}
		if((DirektArcade.Game == "PushMe") || (DirektArcade.Game == "FallingAnvil" || DirektArcade.Game == "MonkeyGame")){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
		if((GameState.isState(GameState.LOBBY)) || (GameState.isState(GameState.PREGAME)) || (GameState.isState(GameState.FINISH))){
			e.setCancelled(true);;
		}
		if((DirektArcade.Game == "PushMe") || (DirektArcade.Game == "FallingAnvil" || DirektArcade.Game == "MonkeyGame")){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e){
		if((GameState.isState(GameState.LOBBY)) || (GameState.isState(GameState.PREGAME)) || (GameState.isState(GameState.FINISH))){
			e.setCancelled(true);;
		}
		if((DirektArcade.Game == "PushMe") || (DirektArcade.Game == "FallingAnvil" || DirektArcade.Game == "MonkeyGame")){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerChat(PlayerChatEvent e){
		Player p = e.getPlayer();
		String m = e.getMessage();
		e.setFormat(GamePrefix.getGamePrefix()+ChatColor.RED+p.getDisplayName()+" > "+ChatColor.RESET+ChatColor.GRAY+m);
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e){
		Player p = e.getPlayer();
		if(!(e.getMessage() != "/start") || !(e.getMessage() != "/vote") || !(e.getMessage() != "/v")){
			if(!p.hasPermission("DirektArcade.admin")){
				e.setCancelled(true);
			}
		}
	}

}
