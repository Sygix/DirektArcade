/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Runnables;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.DirektArcade.DirektArcade;
import com.sygix.DirektArcade.SQL.SQLGestion;
import com.sygix.DirektArcade.Utils.BroadcastMessage;
import com.sygix.DirektArcade.Utils.GamePrefix;
import com.sygix.DirektArcade.Utils.GameState;
import com.sygix.DirektArcade.Utils.SetBlockInRegion;
import com.sygix.DirektArcade.Utils.giveCoins;

public class FallingAnvil {
	
	public static int timetask;
	public static boolean timerun = false;
	public static int time = 300;
	
	public static int stoptimetask;
	public static boolean stoptimerun = false;
	public static int stoptime = 8;
	
	public static int multiplier = 2;
	
	@SuppressWarnings("deprecation")
	public static void start(){
		GameState.setState(GameState.GAME);
		
		try {
			SQLGestion.setState(Bukkit.getServerName(), GameState.getState().name());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		final BossBar bb = Bukkit.createBossBar("", BarColor.RED, BarStyle.SOLID);
		for(Player pls : Bukkit.getOnlinePlayers()){
			pls.teleport(new Location(Bukkit.getWorlds().get(0), -999.5, 4, -999.5));
			pls.setGameMode(GameMode.ADVENTURE);
			pls.getInventory().clear();
			pls.setFoodLevel(20);
			pls.setHealth(20);
			bb.addPlayer(pls);
			for (PotionEffect effect : pls.getActivePotionEffects()){
		        pls.removePotionEffect(effect.getType());
			}
			DirektArcade.PlsNBR.add(pls);
			
			
		}
		if(timerun == true){
			return;
		}
		timerun = true;
		timetask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(DirektArcade.getInstance(), new BukkitRunnable() {
			@Override
			public void run() {
				if(time == 0) {
					bb.setProgress(0.0);
					Timerstop();
					stop();
				}
				long minutes = (time % 3600) / 60;
        		long secondes = (time % 3600 ) % 60;
        		bb.setTitle(ChatColor.GOLD+"Temps restant > "+minutes+":"+secondes);
				if(time == 289 || time == 279 || time == 269 || time == 259 || time == 249 || time == 239 || time == 229 || time == 219 || time == 209 || time == 199 || time == 189 || time == 179 || time == 169 || time == 159 || time == 149 || time == 139 || time == 129 || time == 119 || time == 109 || time == 99 || time == 89 || time == 79 || time == 69 || time == 59 || time == 49 || time == 39 || time == 29 || time == 19 || time == 9 ){
					SetBlockInRegion.minX = -1020;
					SetBlockInRegion.minY = 4;
					SetBlockInRegion.minZ = -1020;
					SetBlockInRegion.maxX = -980;
					SetBlockInRegion.maxY = 10;
					SetBlockInRegion.maxZ = -980;
					SetBlockInRegion.setBlocksInRegion(Material.AIR);
				}
				if(time == 290 || time == 280 || time == 270 || time == 260 || time == 250 || time == 240 || time == 230 || time == 220 || time == 210 || time == 200 || time == 190 || time == 180 || time == 170 || time == 160 || time == 150 || time == 140 || time == 130 || time == 120 || time == 110 || time == 100 || time == 90 || time == 80 || time == 70 || time == 60 || time == 50 || time == 40 || time == 30 || time == 20 || time == 10 ){
					if(time == 290){
						BroadcastMessage.broadcast(GamePrefix.getGamePrefix()+ChatColor.RED+"Effet lenteur "+(multiplier+1)+".");
						for(Player pls : DirektArcade.PlsNBR){
							pls.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, multiplier));
							pls.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, -6));
						}
					}
					for(Player pls : DirektArcade.PlsNBR){
						SetBlockInRegion.minX = pls.getLocation().getBlockX()-1;
						SetBlockInRegion.minY = pls.getLocation().getBlockY()+4;
						SetBlockInRegion.minZ = pls.getLocation().getBlockZ()-1;
						SetBlockInRegion.maxX = pls.getLocation().getBlockX()+1;
						SetBlockInRegion.maxY = pls.getLocation().getBlockY()+4;
						SetBlockInRegion.maxZ = pls.getLocation().getBlockZ()+1;
						SetBlockInRegion.BPworld = pls.getWorld();
						SetBlockInRegion.setBlocksInRegion(Material.ANVIL);
					}
				}
				if(time <= 30){
					bb.setProgress(0.1);
				}else if(time <= 60){
					bb.setProgress(0.2);
				}else if(time <= 90){
					bb.setProgress(0.3);
				}else if(time <= 120){
					bb.setProgress(0.4);
				}else if(time <= 150){
					bb.setProgress(0.5);
				}else if(time <= 180){
					bb.setProgress(0.6);
				}else if(time <= 210){
					bb.setProgress(0.7);
				}else if(time <= 240){
					bb.setProgress(0.8);
				}else if(time <= 270){
					bb.setProgress(0.9);
				}else if(time <= 300){
					bb.setProgress(1.0);
				}
				time--;
				
			}
		}, 20, 20);
	}
	
	@SuppressWarnings("deprecation")
	public static void stop(){
		GameState.setState(GameState.FINISH);
		
		try {
			SQLGestion.setState(Bukkit.getServerName(), GameState.getState().name());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String winner = "";
		if(timerun == true){
			Bukkit.getServer().getScheduler().cancelTask(timetask);
			timerun = false;
		}
		for(Player pls : Bukkit.getServer().getOnlinePlayers()){
			pls.setGameMode(GameMode.SPECTATOR);
			if(DirektArcade.PlsNBR.size() >= 2){
				pls.sendTitle(ChatColor.GOLD+""+ChatColor.BOLD+"La partie est termin�e !", ChatColor.RED+"Match nul !");
				pls.playSound(pls.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1F, 1F);
			}
			if(DirektArcade.PlsNBR.size() < 2){
				winner = DirektArcade.PlsNBR.toString();
				winner = winner.replace("[CraftPlayer{name=", "");
				winner = winner.replace("}]", "");
				pls.sendTitle(ChatColor.GOLD+""+ChatColor.BOLD+"La partie est termin�e !", ChatColor.RED+""+winner+" gagne !");
				pls.playSound(pls.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1F, 1F);
			}
			giveCoins.coins(pls, winner);
		}
		if(stoptimerun == true){
			return;
		}
		stoptimerun = true;
		stoptimetask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(DirektArcade.getInstance(), new BukkitRunnable() {
			@Override
			public void run() {
				if(stoptime == 5){
					SetBlockInRegion.minX = -1020;
					SetBlockInRegion.minY = 4;
					SetBlockInRegion.minZ = -1020;
					SetBlockInRegion.maxX = -980;
					SetBlockInRegion.maxY = 10;
					SetBlockInRegion.maxZ = -980;
					SetBlockInRegion.setBlocksInRegion(Material.AIR);
				}
				if(stoptime == 0) {
					Timerstop();
					stop.Stop();
					return;
				}
				stoptime--;
			}
		}, 20, 20);
	}
	
	public static void Timerstop() {
		if(stoptimerun == true) {
			Bukkit.getServer().getScheduler().cancelTask(stoptimetask);
			stoptimerun = false;
		}
		if(timerun == true){
			Bukkit.getServer().getScheduler().cancelTask(timetask);
			timerun = false;
		}
	}

}
