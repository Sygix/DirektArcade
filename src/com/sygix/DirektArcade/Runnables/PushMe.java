/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Runnables;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.sygix.DirektArcade.DirektArcade;
import com.sygix.DirektArcade.SQL.SQLGestion;
import com.sygix.DirektArcade.Utils.GameState;
import com.sygix.DirektArcade.Utils.giveCoins;

public class PushMe {
	
	public static int timetask;
	public static boolean timerun = false;
	public static int time = 300;
	
	public static int stoptimetask;
	public static boolean stoptimerun = false;
	public static int stoptime = 8;
	
	@SuppressWarnings("deprecation")
	public static void start(){
		GameState.setState(GameState.GAME);
		
		try {
			SQLGestion.setState(Bukkit.getServerName(), GameState.getState().name());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		final BossBar bb = Bukkit.createBossBar("", BarColor.RED, BarStyle.SOLID);
		ItemStack pusher = new ItemStack(Material.BANNER, 1);
	    ItemMeta pushermeta = pusher.getItemMeta();
	    pushermeta.setDisplayName(ChatColor.GREEN + "Pusher");
	    pushermeta.addEnchant(Enchantment.KNOCKBACK, 3, true);
	    pusher.setItemMeta(pushermeta);
		for(Player pls : Bukkit.getServer().getOnlinePlayers()){
			pls.teleport(new Location(Bukkit.getWorlds().get(0), 1000.5, 55, 1000.5));
			pls.setGameMode(GameMode.ADVENTURE);
			pls.getInventory().clear();
			pls.setFoodLevel(20);
			pls.setHealth(20);
			bb.addPlayer(pls);
			pls.setLevel(0);
			for (PotionEffect effect : pls.getActivePotionEffects()){
		        pls.removePotionEffect(effect.getType());
			}
						
			DirektArcade.PlsNBR.add(pls);
		    
		    pls.getInventory().setItem(0, pusher);
		    pls.getInventory().setItem(1, pusher);
		    pls.getInventory().setItem(2, pusher);
		    pls.getInventory().setItem(3, pusher);
		    pls.getInventory().setItem(4, pusher);
		    pls.getInventory().setItem(5, pusher);
		    pls.getInventory().setItem(6, pusher);
		    pls.getInventory().setItem(7, pusher);
		    pls.getInventory().setItem(8, pusher);
		}
		
		if(timerun == true){
			return;
		}
		timerun = true;
		timetask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(DirektArcade.getInstance(), new BukkitRunnable() {
			@Override
			public void run() {
				if(time == 0) {
					bb.setProgress(0.0);
					Timerstop();
					stop();
				}
				long minutes = (time % 3600) / 60;
        		long secondes = (time % 3600 ) % 60;
				bb.setTitle(ChatColor.GOLD+"Temps restant > "+minutes+":"+secondes);
				if(time == 290 || time == 270 || time == 250 || time == 230 || time == 210 || time == 190 || time == 170 || time == 150 || time == 130 || time == 110 || time == 90 || time == 70 || time == 50 || time == 30 || time == 10 ){
					for(Player pls : DirektArcade.PlsNBR){
						pls.setVelocity(new Vector(0, 0.9, 0));
					}
				}
				if(time <= 30){
					bb.setProgress(0.1);
				}else if(time <= 60){
					bb.setProgress(0.2);
				}else if(time <= 90){
					bb.setProgress(0.3);
				}else if(time <= 120){
					bb.setProgress(0.4);
				}else if(time <= 150){
					bb.setProgress(0.5);
				}else if(time <= 180){
					bb.setProgress(0.6);
				}else if(time <= 210){
					bb.setProgress(0.7);
				}else if(time <= 240){
					bb.setProgress(0.8);
				}else if(time <= 270){
					bb.setProgress(0.9);
				}else if(time <= 300){
					bb.setProgress(1.0);
				}
				time--;
			}
		}, 20, 20);
	}
	
	@SuppressWarnings("deprecation")
	public static void stop(){
		GameState.setState(GameState.FINISH);
		
		try {
			SQLGestion.setState(Bukkit.getServerName(), GameState.getState().name());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String winner = "";
		if(timerun == true){
			Bukkit.getServer().getScheduler().cancelTask(timetask);
			timerun = false;
		}
		for(Player pls : Bukkit.getServer().getOnlinePlayers()){
			pls.setGameMode(GameMode.SPECTATOR);
			if(DirektArcade.PlsNBR.size() >= 2){
				pls.sendTitle(ChatColor.GOLD+""+ChatColor.BOLD+"La partie est termin�e !", ChatColor.RED+"Match nul !");
				pls.playSound(pls.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1F, 1F);
			}
			if(DirektArcade.PlsNBR.size() < 2){
				winner = DirektArcade.PlsNBR.toString();
				winner = winner.replace("[CraftPlayer{name=", "");
				winner = winner.replace("}]", "");
				pls.sendTitle(ChatColor.GOLD+""+ChatColor.BOLD+"La partie est termin�e !", ChatColor.RED+""+winner+" gagne !");
				pls.playSound(pls.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1F, 1F);
			}
			giveCoins.coins(pls, winner);
		}
		if(stoptimerun == true){
			return;
		}
		stoptimerun = true;
		stoptimetask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(DirektArcade.getInstance(), new BukkitRunnable() {
			@Override
			public void run() {
				if(stoptime == 0) {
					Timerstop();
					stop.Stop();
					return;
				}
				stoptime--;
			}
		}, 20, 20);
	}
	
	public static void Timerstop() {
		if(stoptimerun == true) {
			Bukkit.getServer().getScheduler().cancelTask(stoptimetask);
			stoptimerun = false;
		}
		if(timerun == true){
			Bukkit.getServer().getScheduler().cancelTask(timetask);
			timerun = false;
		}
	}

}
