/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Runnables;

import java.sql.SQLException;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.DirektArcade.DirektArcade;
import com.sygix.DirektArcade.SQL.SQLGestion;
import com.sygix.DirektArcade.Utils.BroadcastMessage;
import com.sygix.DirektArcade.Utils.GamePrefix;
import com.sygix.DirektArcade.Utils.GameRandom;
import com.sygix.DirektArcade.Utils.GameState;

public class LobbyEnd {
	
	public static int task;
	public static boolean run = false;
	public static int time = 30;
	
	@SuppressWarnings("deprecation")
	public static void Timer(){
		if(run == true){
			return;
		}
		run = true;
		task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(DirektArcade.getInstance(), new BukkitRunnable() {
			@Override
			public void run() {
				for ( Player pls : Bukkit.getServer().getOnlinePlayers()){
					pls.setLevel(time);
					if(time <= 10){
						if(time == 0){
							pls.playSound(pls.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
							Timerstop();
						}else{
							pls.playSound(pls.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1F, 1F);
						}
						IChatBaseComponent comp = ChatSerializer.a("{\"text\": \""+ChatColor.GREEN+"Il reste "+time+" secondes !"+"\"}");
						PacketPlayOutChat paction = new PacketPlayOutChat(comp, (byte) 2);
						((CraftPlayer) pls).getHandle().playerConnection.sendPacket(paction);
					}
				}
				if(time == 30 || time == 15 || time <= 3) {
					if(time == 0) {
						Timerstop();
						VoteChoice();
						GameState.setState(GameState.PREGAME);
						try {
							SQLGestion.setState(Bukkit.getServerName(), GameState.getState().name());
						} catch (SQLException e) {
							e.printStackTrace();
						}
					} else {
						BroadcastMessage.broadcast(GamePrefix.getGamePrefix()+ChatColor.RED+"La partie commence dans : "+time+" sec !");
					}
				}
				time--;
			}
		}, 20, 20);
	}
	
	public static void Timerstop() {
		if(run == true) {
			Bukkit.getServer().getScheduler().cancelTask(task);
			run = false;
		}
	}
	
	public static void VoteChoice(){
		if((DirektArcade.vote1 > DirektArcade.vote2) && (DirektArcade.vote1 > DirektArcade.vote3) && (DirektArcade.vote1 > DirektArcade.vote4)){
			PushMe.start();
			DirektArcade.Game = "PushMe";
		}else if((DirektArcade.vote2 > DirektArcade.vote1) && (DirektArcade.vote2 > DirektArcade.vote3) && (DirektArcade.vote2 > DirektArcade.vote4)){
			FallingAnvil.start();
			DirektArcade.Game = "FallingAnvil";
		}else if((DirektArcade.vote3 > DirektArcade.vote1) && (DirektArcade.vote3 > DirektArcade.vote2) && (DirektArcade.vote3 > DirektArcade.vote4)){
			ElytraRace.start();
			DirektArcade.Game = "ElytraRace";
		}else if((DirektArcade.vote4 > DirektArcade.vote1) && (DirektArcade.vote4 > DirektArcade.vote2) && (DirektArcade.vote4 > DirektArcade.vote3)){
			MonkeyGame.start();
			DirektArcade.Game = "MonkeyGame";
		}else{
			int r = GameRandom.getRandom(1,4);
			if(r == 1){
				PushMe.start();
				DirektArcade.Game = "PushMe";
			}
			if(r == 2){
				FallingAnvil.start();
				DirektArcade.Game = "FallingAnvil";
			}
			if(r == 3){
				ElytraRace.start();
				DirektArcade.Game = "ElytraRace";
			}
			if(r == 4){
				MonkeyGame.start();
				DirektArcade.Game = "MonkeyGame";
			}
		}
	}

}
