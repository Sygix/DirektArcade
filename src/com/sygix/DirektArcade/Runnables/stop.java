/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Runnables;

import java.io.File;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.sygix.DirektArcade.DirektArcade;
import com.sygix.DirektArcade.SQL.SQLGestion;
import com.sygix.DirektArcade.Utils.GameState;

public class stop {
	
	public static void Stop(){
		for(Player pls : Bukkit.getOnlinePlayers()){
			pls.kickPlayer(ChatColor.RED+"La partie est termin�e.");
		}
		for(World w : Bukkit.getWorlds()){
			File playerdata = new File(w.getWorldFolder(), "playerdata");
			File playerstats = new File(w.getWorldFolder(), "stats");
			try{
				for(File f : playerdata.listFiles()){
					f.delete();
				}
				for(File f : playerstats.listFiles()){
					f.delete();
				}
			}catch(Exception e){
			}
		}
		Bukkit.getServer().getScheduler().cancelAllTasks();
		DirektArcade.vote1 = 0;
		DirektArcade.vote2 = 0;
		DirektArcade.vote3 = 0;
		DirektArcade.vote4 = 0;
		DirektArcade.Game = null;
		PushMe.stoptime = 8;
		PushMe.time = 300;
		PushMe.timerun = false;
		PushMe.stoptimerun = false;
		MonkeyGame.stoptime = 8;
		MonkeyGame.time = 300;
		MonkeyGame.height = 1;
		MonkeyGame.timerun = false;
		MonkeyGame.stoptimerun = false;
		FallingAnvil.stoptime = 8;
		FallingAnvil.time = 300;
		FallingAnvil.multiplier = 2;
		FallingAnvil.timerun = false;
		FallingAnvil.stoptimerun = false;
		ElytraRace.stoptime = 8;
		ElytraRace.time = 600;
		ElytraRace.timerun = false;
		ElytraRace.stoptimerun = false;
		LobbyEnd.time = 30;
		LobbyEnd.run = false;
		DirektArcade.PlsNBR.clear();
		DirektArcade.PVoted.clear();
		DirektArcade.maxhalo = 1.0;
		GameState.setState(GameState.LOBBY);
		
		try {
			SQLGestion.setState(Bukkit.getServerName(), GameState.getState().name());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
