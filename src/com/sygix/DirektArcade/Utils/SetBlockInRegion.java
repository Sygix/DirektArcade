/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class SetBlockInRegion {
	
	public static int minX; 
	public static int minY;
	public static int minZ;
	public static int maxX;
	public static int maxY;
	public static int maxZ;
	public static World BPworld = Bukkit.getServer().getWorld("world");
	
	public static void setBlocksInRegion(Material m){
		for(int x = minX; x <= maxX; x++){
			for(int y = minY; y <= maxY; y++){ 
				for(int z = minZ; z <= maxZ; z++){
					Block block = BPworld.getBlockAt(x, y, z);
					block.setType(m);
				}
			}
		}
	}

}
