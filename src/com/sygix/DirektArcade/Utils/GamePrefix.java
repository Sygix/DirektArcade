/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Utils;

import org.bukkit.ChatColor;

public class GamePrefix {
	
	static String prefix = ChatColor.DARK_RED+""+ChatColor.BOLD+"Arcade : ";
	
	public static String getGamePrefix(){
		return prefix;
	}

}
