/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

@SuppressWarnings("deprecation")
public class scoreboard {
	
	private static Scoreboard sb;
	private static Team admin, assist, respdev, respmodo, resparchi, developpeur, staff, moderateur, architecte, helper, videaste, friend, donateur, vip, minivip, joueur;
	
	public static void setteam(){
		sb = Bukkit.getScoreboardManager().getNewScoreboard();
		admin = sb.registerNewTeam("A");
		admin.setPrefix("§4[Admin] ");
		admin.setSuffix(" Ⓢ");
		admin.setNameTagVisibility(NameTagVisibility.ALWAYS);
		assist = sb.registerNewTeam("B");
		assist.setPrefix("§4[A. Admin] ");
		assist.setSuffix(" Ⓢ");
		assist.setNameTagVisibility(NameTagVisibility.ALWAYS);
		respdev = sb.registerNewTeam("C");
		respdev.setPrefix("§c[Resp. Dév] ");
		respdev.setSuffix(" Ⓢ");
		respdev.setNameTagVisibility(NameTagVisibility.ALWAYS);
		respmodo = sb.registerNewTeam("D");
		respmodo.setPrefix("§9[Resp. Modo] ");
		respmodo.setSuffix(" Ⓢ");
		respmodo.setNameTagVisibility(NameTagVisibility.ALWAYS);
		resparchi = sb.registerNewTeam("E");
		resparchi.setPrefix("§5[Resp. Archi] ");
		resparchi.setSuffix(" Ⓢ");
		resparchi.setNameTagVisibility(NameTagVisibility.ALWAYS);
		developpeur = sb.registerNewTeam("F");
		developpeur.setPrefix("§c[Développeur] ");
		developpeur.setSuffix(" Ⓢ");
		developpeur.setNameTagVisibility(NameTagVisibility.ALWAYS);
		staff = sb.registerNewTeam("G");
		staff.setPrefix("§2[Staff] ");
		staff.setSuffix(" Ⓢ");
		staff.setNameTagVisibility(NameTagVisibility.ALWAYS);
		moderateur = sb.registerNewTeam("H");
		moderateur.setPrefix("§9[Modérateur] ");
		moderateur.setSuffix(" Ⓢ");
		moderateur.setNameTagVisibility(NameTagVisibility.ALWAYS);
		architecte = sb.registerNewTeam("I");
		architecte.setPrefix("§5[Architecte] ");
		architecte.setSuffix(" Ⓢ");
		architecte.setNameTagVisibility(NameTagVisibility.ALWAYS);
		helper = sb.registerNewTeam("J");
		helper.setPrefix("§3[Helper] ");
		helper.setSuffix(" Ⓢ");
		helper.setNameTagVisibility(NameTagVisibility.ALWAYS);
		videaste = sb.registerNewTeam("K");
		videaste.setPrefix("§6[Vidéaste] ");
		videaste.setNameTagVisibility(NameTagVisibility.ALWAYS);
		friend = sb.registerNewTeam("L");
		friend.setPrefix("§5[D. Friend] ");
		friend.setNameTagVisibility(NameTagVisibility.ALWAYS);
		donateur = sb.registerNewTeam("M");
		donateur.setPrefix("§3[Donateur] ");
		donateur.setNameTagVisibility(NameTagVisibility.ALWAYS);
		vip = sb.registerNewTeam("N");
		vip.setPrefix("§a[VIP] ");
		vip.setNameTagVisibility(NameTagVisibility.ALWAYS);
		minivip = sb.registerNewTeam("O");
		minivip.setPrefix("§2[Mini-VIP] ");
		minivip.setNameTagVisibility(NameTagVisibility.ALWAYS);
		joueur = sb.registerNewTeam("Z");
		joueur.setPrefix("§7");
		joueur.setNameTagVisibility(NameTagVisibility.ALWAYS);
	}
	
	public static void team(Player p){
		if(p.hasPermission("DirektServ.admin")){
			p.setScoreboard(sb);
			admin.addPlayer(p);
		}else if(p.hasPermission("DirektServ.assist")){
			p.setScoreboard(sb);
			assist.addPlayer(p);
		}else if(p.hasPermission("DirektServ.respdev")){
			p.setScoreboard(sb);
			respdev.addPlayer(p);
		}else if(p.hasPermission("DirektServ.respmodo")){
			p.setScoreboard(sb);
			respmodo.addPlayer(p);
		}else if(p.hasPermission("DirektServ.resparchi")){
			p.setScoreboard(sb);
			resparchi.addPlayer(p);
		}else if(p.hasPermission("DirektServ.developpeur")){
			p.setScoreboard(sb);
			developpeur.addPlayer(p);
		}else if(p.hasPermission("DirektServ.staff")){
			p.setScoreboard(sb);
			staff.addPlayer(p);
		}else if(p.hasPermission("DirektServ.moderateur")){
			p.setScoreboard(sb);
			moderateur.addPlayer(p);
		}else if(p.hasPermission("DirektServ.architecte")){
			p.setScoreboard(sb);
			architecte.addPlayer(p);
		}else if(p.hasPermission("DirektServ.helper")){
			p.setScoreboard(sb);
			helper.addPlayer(p);
		}else if(p.hasPermission("DirektServ.videaste")){
			p.setScoreboard(sb);
			videaste.addPlayer(p);
		}else if(p.hasPermission("DirektServ.friend")){
			p.setScoreboard(sb);
			friend.addPlayer(p);
		}else if(p.hasPermission("DirektServ.donateur")){
			p.setScoreboard(sb);
			donateur.addPlayer(p);
		}else if(p.hasPermission("DirektServ.vip")){
			p.setScoreboard(sb);
			vip.addPlayer(p);
		}else if(p.hasPermission("DirektServ.minivip")){
			p.setScoreboard(sb);
			minivip.addPlayer(p);
		}else{
			p.setScoreboard(sb);
			joueur.addPlayer(p);
		}
	}

}
