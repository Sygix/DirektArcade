/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Utils;

public enum GameState {
	
	LOBBY(true), PREGAME(false), GAME(false), FINISH(false);
	
	private static GameState currentState;
	private boolean canJoin;
	
	GameState(boolean s) {
		canJoin = s;
	}
	
	public static void setState(GameState s) {
		currentState = s;
	}
	
	public static boolean isState(GameState s) {
		return currentState == s;
	}
	
	public static GameState getState() {
		return currentState;
	}
	
	public boolean canJoin() {
		return canJoin;
	}
}
