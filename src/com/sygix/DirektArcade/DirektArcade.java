/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.sygix.DirektArcade.Commands.hshareCommand;
import com.sygix.DirektArcade.Commands.reloadCommand;
import com.sygix.DirektArcade.Commands.startCommand;
import com.sygix.DirektArcade.Commands.voteCommand;
import com.sygix.DirektArcade.Events.EventManager;
import com.sygix.DirektArcade.SQL.SQLGestion;
import com.sygix.DirektArcade.Utils.GameState;
import com.sygix.DirektArcade.Utils.scoreboard;


public class DirektArcade extends JavaPlugin{
	
	public static int vote1;
	public static int vote2;
	public static int vote3;
	public static int vote4;
	public static double maxhalo = 1.0;
	public static ArrayList<Player> PlsNBR = new ArrayList<Player>();
	public static String Game;
	public static ArrayList<Player> PVoted = new ArrayList<Player>();
	public static DirektArcade instance;
	
	public static DirektArcade getInstance() {
		return instance;
	}
	
	@Override
	public void onEnable(){
		getLogger().info("Chargement ...");
		instance = this;
		EventManager.registerEvent(this);
		scoreboard.setteam();
		getCommand("vote").setExecutor(new voteCommand());
		getCommand("start").setExecutor(new startCommand());
		getCommand("reload").setExecutor(new reloadCommand());
		getCommand("hshare").setExecutor(new hshareCommand());
		try {
			SQLGestion.connect();
			SQLGestion.registerPlugin();
			SQLGestion.registerServerOnBDD(Bukkit.getServerName());
			SQLGestion.setState(Bukkit.getServerName(), GameState.LOBBY.name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		GameState.setState(GameState.LOBBY);
		getLogger().info("Chargement termine !");
	}
	
	@Override
	public void onDisable(){
		getLogger().info("Dechargement ...");
		for(World w : Bukkit.getWorlds()){
			File folder = new File(w.getWorldFolder(), "playerdata");
			for(File f : folder.listFiles()){
				f.delete();
			}
		}
		try {
			SQLGestion.setState(Bukkit.getServerName(), "DOWN");
			SQLGestion.disconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		getLogger().info("Dechargement termine !");
	}

}
