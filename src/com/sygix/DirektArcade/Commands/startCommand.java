/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sygix.DirektArcade.Runnables.LobbyEnd;
import com.sygix.DirektArcade.Utils.GameState;

public class startCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		Player p = (Player) s;
		if(GameState.isState(GameState.LOBBY)){
			if(p.hasPermission("Direkt.start")){
				LobbyEnd.Timerstop();
				LobbyEnd.VoteChoice();
				GameState.setState(GameState.PREGAME);
				p.sendMessage(ChatColor.AQUA+"Vous avez d�marr� la partie.");
				for(Player pls : Bukkit.getServer().getOnlinePlayers()){
					if(pls != p){
						pls.sendMessage(ChatColor.AQUA+""+p.getDisplayName()+" a d�marr� la partie.");
					}
				}
			}
		}
		return false;
	}

}
