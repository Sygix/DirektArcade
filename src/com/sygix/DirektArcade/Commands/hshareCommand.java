/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Commands;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sygix.DirektArcade.DirektArcade;
import com.sygix.DirektArcade.SQL.SQLGestion;

public class hshareCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player p = (Player) sender;
		try {
			double phalo = SQLGestion.getHalo(p);
			if(phalo > DirektArcade.maxhalo){
				DirektArcade.maxhalo = phalo;
				for(Player pls : Bukkit.getOnlinePlayers()){
					pls.sendTitle(ChatColor.GOLD+""+p.getDisplayName()+" vient d'augmenter le halo !", ChatColor.AQUA+""+ChatColor.ITALIC+"Nouveau halo : x"+phalo);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			p.sendMessage(ChatColor.RED+"Une Erreur[ShareHalo] s'est produite, contactez un développeur.");
		}
		return false;
	}

}
