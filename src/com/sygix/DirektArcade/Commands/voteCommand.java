/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektArcade.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sygix.DirektArcade.DirektArcade;

public class voteCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		if((s.getName() != null) && (s.getName() != "CONSOLE")){
			Player p = (Player) s;
			if(args.length == 1){
				if(!DirektArcade.PVoted.contains(p)){
					if(args[0].equalsIgnoreCase("1")){
						DirektArcade.vote1++;
						DirektArcade.PVoted.add(p);
						p.sendMessage(ChatColor.YELLOW+"Vous venez de voter pour \"PushMe\" !");
					}
					if(args[0].equalsIgnoreCase("2")){
						DirektArcade.vote2++;
						DirektArcade.PVoted.add(p);
						p.sendMessage(ChatColor.YELLOW+"Vous venez de voter pour \"FallingAnvil\" !");
					}
					if(args[0].equalsIgnoreCase("3")){
						DirektArcade.vote3++;
						DirektArcade.PVoted.add(p);
						p.sendMessage(ChatColor.YELLOW+"Vous venez de voter pour \"ElytraRace\" !");
					}
					if(args[0].equalsIgnoreCase("4")){
						DirektArcade.vote4++;
						DirektArcade.PVoted.add(p);
						p.sendMessage(ChatColor.YELLOW+"Vous venez de voter pour \"MonkeyGame\" !");
					}
				}else {
					p.sendMessage(ChatColor.YELLOW+"Vous avez d�j� vot� !");
				}
			}else {
				p.sendMessage(ChatColor.YELLOW+"/v <chiffre>");
			}
		}else{
			s.sendMessage(ChatColor.YELLOW+"Vous devez etre un joueur !");
		}
		return false;
	}

}
